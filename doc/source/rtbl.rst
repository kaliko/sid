Real time block list
--------------------

The real time block list plugin can be used easily calling the module:

.. code:: bash

    # Fetch the code
    git clone -b dev https://gitlab.com/kaliko/sid.git
    cd sid
    # Create the virtual env
    python3 -m venv --prompt "rtbl-bot" venv
    . ./venv/bin/activate
    pip install -U pip wheel slixmpp==1.8.3
    # Smoke test
    python3 -m sid.rtbl --help


Then run, within the same virtual env:

.. code:: bash

    # Activate the previously created virtual env
    . ./venv/bin/activate
    # Run the bot
    python3 -m sid.rtbl --jid botjid@example.org \
                        --room muc_to_moderate@conf.example.org \
                        --rtbl xmppbl.org

.. vim: spell spelllang=en
