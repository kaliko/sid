# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText:  2014, 2020 kaliko <kaliko@azylum.org>
# SPDX-License-Identifier: GPL-3.0-or-later

from .plugin import Plugin, botcmd


class Ping(Plugin):
    PongURL = 'http://upload.wikimedia.org/wikipedia/commons/f/f8/Pong.png'

    def __init__(self, bot):
        Plugin.__init__(self, bot)

    @botcmd
    def ping(self, rcv, args):
        """Answers a pong showing the bot is alive.

        !ping : You'll get back "pong"!
        """
        msg = {'mhtml': '!<a href="{0.PongURL}">pong</a>'.format(self),
               'mbody': '!pong'}
        self.reply(rcv, msg)

# VIM MODLINE
# vim: ai ts=4 sw=4 sts=4 expandtab
