# -*- coding: utf-8 -*-
# SPDX-FileCopyrightText: 2015, 2021 kaliko <kaliko@azylum.org>
# SPDX-FileCopyrightText: 2010, 2011 Anaël Verrier <elghinn@free.fr>
# SPDX-License-Identifier: GPL-3.0-or-later
"""Intercepts bugs numbers in MUC messages and send info about it

>>> from sid.bts import Bugs
"""

from re import compile as re_compile

import debianbts

from .plugin import Plugin, botcmd


class Bugs(Plugin):
    """Gets bugs info from the BTS

    .. note::
      This plugin depends on external module: **python-debianbts**
    """
    #: Bug id regexp, intercepts bug id in strings : "#629234", "bugs.debian.org/629234" and "bugreport.cgi?bug=629234"
    re_bugs = re_compile(r'(?:(?<=#)|(?<=bugreport\.cgi\?bug=)|(?<=bugs\.debian\.org/))(\d{6,7})')
    #: Package name regexp
    re_pkg = re_compile(r'(?P<package>[0-9a-z.+-]+)$')

    def __init__(self, bot):
        Plugin.__init__(self, bot)
        bot.add_event_handler("muc::%s::message" %
                              self.bot.room, self.muc_message)

    def muc_message(self, msg):
        """Handler method dealing with MUC incoming messages.

        Intercepts bugs number in MUC messages (as #629234), replies a bug
        summary."""
        # Does not reply to myself
        if msg['mucnick'] == self.bot.nick:
            return
        bugs = list()
        for bug_id in set(Bugs.re_bugs.findall(msg['body'].strip())):
            self.log.debug('got bug id: %s', bug_id)
            query = debianbts.get_status(bug_id)
            if len(query) == 1:
                bug = query[0]
                url = debianbts.BTS_URL + bug_id
                bugs.append({'id': bug_id,
                             'package': bug.package,
                             'summary': bug.subject,
                             'url': url})
            else:
                self.log.warning('Wrong bug number "%s"?', bug_id)
                bugs.append({'id': bug_id})
        for bug in bugs:
            if len(bug) == 1:
                message = 'Invalid bug id: {id}'.format(**bug)
                self.reply(msg, message)
            else:
                message = {'mhtml': '<a href="%(url)s">#%(id)s</a>: %(package)s “ %(summary)s ”' % bug,
                           'mbody': '#%(id)s: %(package)s “ %(summary)s ” %(url)s' % bug}
                self.reply(msg, message)

    @botcmd
    def bugs(self, rcv, args):
        """Gets bugs info from the BTS

        ``!bugs pkg-name`` : Returns latest bug reports if any
        """
        if not args:
            return
        if len(args) > 1:
            self.log.info('more than one packages provided')
        pkg = Bugs.re_pkg.match(args[0])
        if not pkg:
            msg = 'Wrong package name format re: "{}"'.format(Bugs.re_pkg.pattern)
            self.reply(rcv, msg)
            return
        reports_ids = debianbts.get_bugs(status='open', **pkg.groupdict())
        if not reports_ids:
            self.reply(rcv, 'No open bugs for "{}"'.format(pkg.string))
            return
        reports = debianbts.get_status(reports_ids)
        reports = sorted(reports, key=lambda r: r.date)
        msg = ['Latest reports for {1} (total {0})'.format(len(reports), pkg.string)]
        # Reverse and take last reports
        for rep in reports[::-1][:4]:
            msg.append('{r.bug_num}: {r.date:%Y-%m-%d} {r.subject}'.format(r=rep))
        message = {'mbody': '\n'.join(msg)}
        self.reply(rcv, message)
