# coding: utf-8
# SPDX-FileCopyrightText:  2020 kaliko <kaliko@azylum.org>
# SPDX-License-Identifier: GPL-3.0-or-later

import json
import logging

from urllib.error import URLError, HTTPError
from urllib.request import Request, urlopen
from urllib.parse import urlencode


def get_pkg(pkg):
    """
    Uses the DAK HTTP api
    https://ftp-team.pages.debian.net/dak/epydoc/dakweb.queries.madison-module.html
    """
    logger = logging.getLogger(__package__)
    data = {}
    data['package'] = pkg
    data['f'] = ''
    values = urlencode(data)
    url = f'https://api.ftp-master.debian.org/madison?{values}'
    logger.debug(url)
    req = Request(url, method='GET')
    try:
        response = urlopen(req)
        ans = response.read()
    except HTTPError as err:
        logger.info('The server couldn\'t fulfill the request.')
        logger.debug('Error code: %s', err.code)
    except URLError as err:
        logger.info('We failed to reach a server.')
        logger.debug('Reason: %s', err.reason)
    try:
        pkg_info = json.loads(ans)
    except Exception as err:
        logger.error('Failed to decode json')
    if not pkg_info:
        return []
    return pkg_info[0][pkg]


# Script starts here
if __name__ == '__main__':
    pass

# VIM MODLINE
# vim: ai ts=4 sw=4 sts=4 expandtab fileencoding=utf8
